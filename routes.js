Router.route('/', {
    name: 'home',
    template: 'home'
});

Router.route('/login', {
    name: 'login',
    template: 'login'
});

Router.route('/register', {
    name: 'register',
    template: 'register'
});

Router.route('/list/:_id', {
    name: 'listPage',
    template: 'listPage',
    data: function() {
        console.log(this.params);
        var currentList = this.params._id;
        var currentUser = Meteor.userId();
        return Lists.findOne({ _id: currentList, createdBy: currentUser });
    },
    onBeforeAction: function() {
        var currentUser = Meteor.userId();
        if (currentUser) {
            this.next();
        } else {
            Router.go("login");
        }
    }
});

Router.route('/todos', {
    name: 'todos',
    template: 'todos'
});

Router.configure({
    layoutTemplate: 'main'
});
