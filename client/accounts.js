if(Meteor.isClient){
    // client code goes here

    Template.login.onCreated(function() {
        console.log("The 'login' template was just created.");
    });

    Template.login.onRendered(function() {
        var validator = $('.login').validate({
            submitHandler: function(event) {
                var email = $('[name=email]').val();
                var password = $('[name=password]').val();

                Meteor.loginWithPassword(email, password, function(error) {
                    if (error) {
                        // if(error.reason == "User not found"){
                        //     validator.showErrors({
                        //         // email: error.reason
                        //         email: "Email not found in the database"
                        //     });
                        // }
                        // if(error.reason == "Incorrect password"){
                        //     validator.showErrors({
                        //         // password: error.reason
                        //         password: "You entered an incorrect password"
                        //     });
                        // }
                        var confirm = window.confirm(error.reason+"!");
                    } else {
                        Router.go("home");
                    }
                });
            }
        });
        console.log("The 'login' template was just rendered.");
    });

    Template.login.onDestroyed(function() {
        console.log("The 'login' template was just destroyed.");
    });

    Template.register.onRendered(function(){
        var validator = $('.register').validate({
            submitHandler: function(event){
                var email = $('[name=email]').val();
                var password = $('[name=password]').val();

                Accounts.createUser({
                    email: email,
                    password: password
                }, function(error){
                    if(error){
                        var confirm = window.confirm(error.reason+"!");
                    } else {
                        Router.go("home");
                    }
                });
            }
        });
    });

    Template.register.events({
        'submit form': function(event){
            event.preventDefault();
            // var email = $('[name=email]').val();
            // var password = $('[name=password]').val();
            //
            // Accounts.createUser({
            //     email: email,
            //     password: password
            // });
            //
            // Router.go('home');
        }
    });

    Template.login.events({
        'submit form': function(event){
            event.preventDefault();
            // var email = $('[name=email]').val();
            // var password = $('[name=password]').val();
            // Meteor.loginWithPassword(email, password, function(error) {
            //     if (error) {
            //         console.log(error.reason);
            //     } else {
            //         Router.go("home");
            //     }
            // });
        }
    });

    Template.navigation.events({
        "click .logout": function(event) {
            event.preventDefault();
            Meteor.logout();
            Router.go('login');
        }
    });
}
