Lists = new Meteor.Collection('lists');

if (Meteor.isClient) {

    Template.lists.helpers({
        "list": function() {
            return Lists.find({}, { sort: { name: 1 } });
        }
    });

    Template.addList.events({
        "submit form": function(event) {
            event.preventDefault();
            var listName = $('[name=listName]').val();
            var currentUser = Meteor.userId();
            Lists._collection.insert({
                name: listName
            }, function(error, results) {
                Router.go('listPage', { _id: results });
            });
            // clear form
            $('[name=listName]').val('');
        }
    });

}
