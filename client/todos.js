Todos = new Mongo.Collection('todos');

if (Meteor.isClient) {
    // client code goes here

    Template.todos.helpers({
        'todo': function(){
            // return Todos.find();
            var currentList = this._id;
            return Todos.find({ listId: currentList }, { sort: { createdAt: -1 } });
        }
    });

    Template.todoItem.helpers({
        "checked": function() {
            var isCompleted = this.completed;
            if (isCompleted) {
                return "checked";
            } else {
                return "";
            }
        }
    });

    Template.todosCount.helpers({
        "totalTodos": function() {
            var currentList = this._id;
            return Todos.find({ listId: currentList }).count();
        },
        "completedTodos": function() {
            var currentList = this._id;
            return Todos.find({ listId: currentList, completed: true }).count();
        }
    });

    Template.addTodo.events({
        "submit form": function(event){
            event.preventDefault();
             var todoName = $('[name="todoName"]').val();
             var currentList = this._id;
             Todos._collection.insert({
                name: todoName,
                completed: false,
                createdAt: new Date(),
                listId: currentList
             });
             // clear form
             $('[name="todoName"]').val('');
        }
    });

    Template.todoItem.events({
        "click .delete-todo": function(event) {
             event.preventDefault();
             var documentId = this._id;
             var confirm = window.confirm("Delete this task?");
             if (confirm) {
                 Todos._collection.remove({ _id: documentId });
             }
        },
        "keyup [name=todoItem]": function(event) {
            if(event.which == 13 || event.which == 27){
                $(event.target).blur();
            } else {
                var documentId = this._id;
                var todoItem = $(event.target).val();
                Todos._collection.update(
                    { _id: documentId },
                    { $set: { name: todoItem } }
                );
                console.log(event.which);
            }
        },
        "change [type=checkbox]": function() {
            var documentId = this._id;
            var isCompleted = this.completed;
            if (isCompleted) {
                Todos._collection.update(
                    { _id: documentId },
                    { $set: { completed: false } }
                );
                console.log("Task marked as incomplete.");
            } else {
                Todos._collection.update(
                    { _id: documentId },
                    { $set: { completed: true } }
                );
                console.log("Task marked as complete.");
            }
        }
    });

}
